# NewsApp

Sample web app serves good news.

## Global Dependencies

- Node 10.x & NPM 6.x (Download here https://nodejs.org/en/download/ and follow its install instruction)

- Angular CLI 8.x (Run `npm install ng -g` after installing of Node & NPM above)

## Quick way to see this demo in action

- Run `npm run build`

- Run `npm run start`

- Navigate `http://localhost:8080/index.html`

*This is for demonstration purpose only - this is not intended to be used in production environment.*

## Start Development

- Run `npm run dev` or `ng serve`

- Navigate to `http://localhost:4200/`

- The app will automatically reload if you change any of the source files.

- Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build Production

- Run `npm run build` or `ng build`

- The build artifacts will be stored in the `dist/` directory

- Serve the `dist` directory with any HTTP server

## Test

- Run `npm run test` or `ng test`

- The browser window will be opened and result of test cases will be reported

- Test cases are defined in *.spec.ts files following stardard of Jasmine Unit Test 

## Notes

**There are differencies in pixels between the original mockups and the expected values.**

In this implementation, I followed the expected values (px values in red) although I see that it does not look good at all. Hope you acknowledge this!

**The mock data, logo, favicon resources are for demo purpose which can be replaced by real ones if requested.**

**This project does not target IE 9 - 11 due to these are outdated browsers but I can support them if requested.**
