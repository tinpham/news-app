import { Component, OnInit, OnDestroy } from '@angular/core'
import { AppService } from '../app.service'
import { Subscription } from 'rxjs'
import { News } from '../app.model'

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit, OnDestroy {

  items: Array<News> = []
  currentPage = 0
  pageSize = 15
  hasMore = false

  searching: string
  loadSubscription: Subscription
  searchSubscribtion: Subscription

  constructor(private appService: AppService) { }

  ngOnInit() {
    this.load()
    this.searchSubscribtion = this.appService.search
      .subscribe(value => {
        this.items = []
        this.currentPage = 0
        this.hasMore = false
        this.searching = value
        this.load()
      })
  }

  ngOnDestroy() {
    if (this.loadSubscription) {
      this.loadSubscription.unsubscribe()
    }
    if (this.searchSubscribtion) {
      this.searchSubscribtion.unsubscribe()
    }
  }

  load() {
    if (this.loadSubscription) {
      this.loadSubscription.unsubscribe()
    }
    this.loadSubscription = this.appService.getNews(this.currentPage, this.pageSize, this.searching)
      .subscribe(news => {
        this.items = this.items.concat(news)
        this.hasMore = news.length < this.pageSize ? false : true
        this.currentPage++
      })
  }
}
