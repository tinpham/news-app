import { Component, OnInit, OnDestroy } from '@angular/core'
import { Router, NavigationEnd } from '@angular/router'
import { Subscription } from 'rxjs'
import { filter } from 'rxjs/operators'
import { AppService } from '../app.service'
import { Title } from '@angular/platform-browser'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  subscription: Subscription
  title: string
  caption: string
  name: string
  navBoxHidden: boolean
  searchable: boolean
  links: Array< { name: string, value: string } >

  constructor(
    private router: Router,
    private appService: AppService,
    private titleService: Title) { }

  ngOnInit() {
    this.name = 'Logo'
    this.updateMeta(this.router.url)
    this.navBoxHidden = true
    this.searchable = false
    this.links = [
      { name: 'News', value: '/news' },
      { name: 'Regions', value: '/regions' },
      { name: 'Video', value: '/video' },
      { name: 'TV', value: '/tv' }
    ]
    this.subscription = this.router.events
      .pipe(
        filter((event: any) => event instanceof NavigationEnd)
      )
      .subscribe(event => {
        this.updateMeta(event.url)
      })
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe()
    }
  }

  updateMeta(url) {
    switch (url) {
      case '/':
      case '/news':
        this.title = 'Home of Good News'
        this.caption = 'News'
        break
      case '/regions':
        this.title = 'Hot in Your Regions'
        this.caption = 'Regions'
        break
      case '/video':
        this.title = 'Watching Hot Videos'
        this.caption = 'Video'
        break
      case '/tv':
        this.title = 'Latest on TV'
        this.caption = 'TV'
        break
      default:
        this.title = 'Sorry'
        this.caption = 'Unavailable'
        break
    }
    this.titleService.setTitle(this.title)
  }

  toggleNavBox() {
    this.navBoxHidden = !this.navBoxHidden
  }

  tapLink(url) {
    this.toggleNavBox()
    this.router.navigateByUrl(url)
  }

  fireSearch(value) {
    if (!value || value.trim().length === 0) {
      this.searchable = false
    }
    this.appService.search.emit(value)
  }

  isLinkActive(value) {
    return value === this.router.url
  }

  onSearchChange() {
    this.searchable = true
  }
}
