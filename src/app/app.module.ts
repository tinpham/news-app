import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { RootComponent } from './root/root.component'
import { HeaderComponent } from './header/header.component'
import { FooterComponent } from './footer/footer.component'
import { NewsComponent } from './news/news.component'
import { RegionsComponent } from './regions/regions.component'
import { VideoComponent } from './video/video.component'
import { TvComponent } from './tv/tv.component'
import { UnavailableComponent } from './unavailable/unavailable.component'
import { ContainerComponent } from './container/container.component'

const routes: Routes = [
  { path: '', component: NewsComponent },
  { path: 'news', component: NewsComponent },
  { path: 'regions', component: RegionsComponent },
  { path: 'video', component: VideoComponent },
  { path: 'tv', component: TvComponent },
  { path: '**', component: UnavailableComponent }
]

@NgModule({
  declarations: [
    RootComponent,
    HeaderComponent,
    FooterComponent,
    NewsComponent,
    RegionsComponent,
    VideoComponent,
    TvComponent,
    UnavailableComponent,
    ContainerComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [RootComponent]
})
export class AppModule { }
