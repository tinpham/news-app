export class News {
  uri: string
  title: string
  description: string
  cover: string
  meta: {
    author: string
    updated_date: string
  }
}
