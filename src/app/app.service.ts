import { Injectable, EventEmitter } from '@angular/core'
import { of, Observable } from 'rxjs'
import { News } from './app.model'

const monthNames = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
]

@Injectable({
  providedIn: 'root'
})
export class AppService {

  search = new EventEmitter<string>()
  news: Array<News> = []

  constructor() {
    this.initNews()
  }

  getNews(pageIndex: number, pageSize: number, searchValue?: string): Observable<News[]> {
    const items = searchValue && searchValue.trim().length > 0
      ? this.news.filter((item) =>
          item.title.toLowerCase().includes(searchValue.toLowerCase())
          || item.description.toLowerCase().includes(searchValue.toLowerCase())
        )
      : this.news
    const pages = this.toChunks(items, pageSize)
    const results = pages[pageIndex]
    return of(results || [])
  }

  // These functions below are just for generating mock data

  private toChunks(array: Array<any>, size: number): Array<any> {
    const chunks = []
    let i = 0
    const n = array.length
    while (i < n) {
      chunks.push(array.slice(i, i += size))
    }
    return chunks
  }

  private displayDate(date: Date) {
    return `
      ${date.getDate()} ${monthNames[date.getMonth()]}, ${date.getFullYear()}
      ${date.toLocaleTimeString(navigator.language, {hour: '2-digit', minute: '2-digit'})}
    `
  }

  private initNews() {
    for (let i = 0; i < 32; i++) {
      this.news.push({
        uri: `${i}`,
        title: i % 2 ? `What is Lorem Ipsum?` : `Search me please!`,
        description: `
          Lorem Ipsum is simply dummy text of the printing and typesetting industry.
          Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
          when an unknown printer took a galley of type and scrambled it to make a type
          specimen book. It has survived not only five centuries, but also the leap
          into electronic typesetting, remaining essentially unchanged.`,
        cover: '/assets/images/image-placeholder.jpg',
        meta: {
          author: 'Tin Pham',
          updated_date: this.displayDate(new Date())
        }
      })
    }
  }
}
